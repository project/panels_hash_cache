<?php

/**
 * @file
 * Drush integration for the Panels Hash Cache module.
 */

/**
 * Implements hook_drush_cache_clear().
 *
 * Adding cache clear option for panels_hash_cache.
 */
function panels_hash_cache_drush_cache_clear(&$types) {
  $types['panels-hash-cache'] = 'panels_hash_cache_clear';
}

/**
 * Invalidate the panel hash cache.
 *
 * @see cache_clear_all()
 */
function panels_hash_cache_clear() {
  cache_clear_all('panels-hash-cache', 'cache_panels', TRUE);
}
